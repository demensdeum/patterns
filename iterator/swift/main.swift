import Foundation

enum IteratorError: Error {
    case runtimeError(String)
}

class Iterator<T> {
    open func next() -> T? { return nil }
}

protocol IterableCollection {
    associatedtype K
    func iterator() -> Iterator<K>
}

struct Track: Hashable {
    var band: String
    var title: String
    var bpm: Int?
}

struct Filter {
    let key: String
    let value: Any
}

protocol TracksIteratorDataSource: class {
    func tracksIterator(_ iterator: TracksIterator, trackAtIndex index: Int, withIterableCollectionUpdateDate date: Date, filters: [Filter]) throws -> Track?
}

class TracksIterator: Iterator<Track> {
    let filters: [Filter]
        
    private var index: Int
    private var iterableCollectionUpdateDate: Date
    
    private weak var dataSource: TracksIteratorDataSource?
    
    
    init(filters: [Filter] = [], index: Int, iterableCollectionUpdateDate: Date, dataSource: TracksIteratorDataSource) {
        self.filters = filters
        self.index = index
        self.iterableCollectionUpdateDate = iterableCollectionUpdateDate
        self.dataSource = dataSource
    }
    
    override func next() -> Track? {
        index += 1
        
        var track: Track? = nil
        
        do {
            try track = dataSource?.tracksIterator(self, trackAtIndex: index, withIterableCollectionUpdateDate: iterableCollectionUpdateDate, filters: filters)
        }
        catch {
            print(error)
            exit(1)
        }
        
        return track
    }
}

class TracksCollection {
    var undergroundCollectionTracks: [Track] = [] {
        didSet {
            updateDate = Date()
        }
    }
    
    var cassetsTracks: [Track] = [] {
        didSet {
            updateDate = Date()
        }
    }
    
    var updateDate: Date = Date()
    
    func add(undergroundTrack: Track) {
        undergroundCollectionTracks.append(undergroundTrack)
    }
    
    func add(cassetTrack: Track) {
        cassetsTracks.append(cassetTrack)
    }
    
    func filterableIterator(filters: [Filter]) -> Iterator<Track> {
        return TracksIterator(filters: filters, index: -1, iterableCollectionUpdateDate: updateDate, dataSource: self)
    }
}

extension TracksCollection: IterableCollection {
    func iterator() -> Iterator<Track> {
        return filterableIterator(filters: [])
    }
}

extension TracksCollection: TracksIteratorDataSource {
    func tracksIterator(_ iterator: TracksIterator, trackAtIndex index: Int, withIterableCollectionUpdateDate date: Date, filters: [Filter]) throws -> Track? {
        guard index >= 0 else { return nil }
        guard date == updateDate else { throw IteratorError.runtimeError("Collection changed between iterations") }
        
        var allTracks: [Track] = []
        allTracks.append(contentsOf: cassetsTracks)
        allTracks.append(contentsOf: undergroundCollectionTracks)
        
        var filteredTracks = allTracks
        
        if let bandFilter = filters.first(where: {$0.key == "band"}), let band = bandFilter.value as? String {
            filteredTracks.removeAll(where: {$0.band != band})
        }
        
        if let bpmFilter = filters.first(where: {$0.key == "bpm"}), let bpm = bpmFilter.value as? Int {
            filteredTracks.removeAll(where: {$0.bpm != bpm })
        }
                
        var filteredSet: Set<Track> = Set<Track>()
        filteredTracks.forEach { filteredSet.insert($0) }

        let filteredUnique = Array(filteredSet)
        
        guard index < filteredUnique.count else { return nil }        
        
        return filteredUnique[index]
    }
}

let tracksCollection = TracksCollection()
tracksCollection.add(undergroundTrack: Track(band: "Djentuggah", title: "Orangeworks", bpm: 140))
tracksCollection.add(undergroundTrack: Track(band: "Djentuggah", title: "Bleen", bpm: 160))
tracksCollection.add(undergroundTrack: Track(band: "Emo for my St.Patrick", title: "Ballad of Blood Eyes", bpm: 100))

let bandFilter = Filter(key: "band", value: "Djentuggah")
let bpmFilter = Filter(key: "bpm", value: 140)
let iterator = tracksCollection.filterableIterator(filters: [bandFilter, bpmFilter])

while let track = iterator.next() {
    print("\(track.band) - \(track.title)")
}
