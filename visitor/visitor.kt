open class Band {
	open fun accept(visitor: Visitor) {
		visitor.visit(this)
	}
}

class MurpleDeep: Band() {
	override fun accept(visitor: Visitor) {
		visitor.visit(this)
	}
}

class Visitor {
	fun visit(band: Band) {
		println("This is Band class")
	}

	fun visit(band: MurpleDeep) {
		println("This is MurpleDeep class")
	}
}

fun printout(band: Band) {
	val visitor = Visitor()
	band.accept(visitor)
}

fun main(args: Array<String>) {
	val deepMurple = MurpleDeep()
	printout(deepMurple)
} 
