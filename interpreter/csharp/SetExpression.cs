class SetExpression : TerminalExpression
{
    private String key;    
    private String value;

    public SetExpression(String key, String value) {
        this.key = key;
        this.value = value;
    }
    override public String interpret(Context context) {
        context.variables[key] = value;
        return "ok";
    }
}