class NonTerminalExpression : IExpression
{
    protected IExpression[] leafs;

    public NonTerminalExpression(IExpression[] leafs) {
        this.leafs = leafs;
    }

    virtual public String interpret(Context context) {
        return "NonTerminalExpression interpret(context) default implementation";
    }
}