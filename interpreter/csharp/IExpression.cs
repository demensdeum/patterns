interface IExpression
{
    String interpret(Context context);
}