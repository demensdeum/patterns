class GetExpression : TerminalExpression
{
    private String key;    

    public GetExpression(String key) {
        this.key = key;
    }
    override public String interpret(Context context) {
        return context.variables[key];
    }
}