class PrintExpression : NonTerminalExpression
{
    public PrintExpression(IExpression[] leafs) : base(leafs) {
        this.leafs = leafs;
    }
    
    override public String interpret(Context context) {
        if (leafs.Length < 1) {
            return "ERROR: Print expression leafs Length < 1";
        }
        else if (leafs.Length > 1) {
            return "ERROR: Print expression leafs Length > 1";
        }
        return leafs[0].interpret(context);
    }
}