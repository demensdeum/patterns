module Main exposing (..)

import Context exposing (..)
import Result exposing (..)
import Browser
import Html exposing (Html, Attribute, button, div, text, input)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import ProgramCodeParser exposing (toAST)
import Stack exposing (..)
import Expression exposing (..)
import Dict exposing (..)
import Interpreter exposing (..)

initialProgram = "Print Perform( \"Hello\" \"⠀\" \"World\" )"
-- ok: initialProgram = "Perform( Set alpha 1 Get alpha )"
-- ok: initialProgram = "Print Perform( Perform( Alpha Beta ) ;⠀Hello⠀World;⠀ Perform( Gamma Perform( Omega Void ) ) )"
-- ok: initialProgram = "Perform( Perform( \"1\" \"2\" ) Perform( \"3\" Perform( \"5\" \"0\" ) ) )"
-- ok: initialProgram = "Perform( \"Hello\" \"World\" )"
-- ok: initialProgram = "Perform( \"Hello\" )"
-- ok: initialProgram = "Print \"Hello\""


main : Program () Model Msg
main = Browser.sandbox {
  init = init
  , update = update
  , view = view
  }

type alias Model =
    {
      program : String,
      output : String,
      ast : String
    }

init : Model
init =
    {
    program = initialProgram,
    output = "[Program output]",
    ast = "[AST]"
    }

type Msg =
  RunProgram |
  TextChange String

textChange: String -> Model
textChange text =
  {
    program = text,
    output = "Program code changed",
    ast = "Program code changed"
  }

runProgram: Model -> Model
runProgram model =
  let ast = ProgramCodeParser.toAST model.program in
    let output = (run { expression = ast, context = { variables = Dict.empty } } ).output in
      {
        program = model.program,
        output = "Output: " ++ output,
        ast = "AST: " ++ Debug.toString(ast)
      }

update : Msg -> Model -> Model
update msg model =
  case msg of
    RunProgram -> runProgram model
    TextChange text -> textChange text

view : Model -> Html Msg
view model =
  div []
    [
    div [] [ text model.ast ],      
    div [] [ text model.output ],
    div [] [ input [ placeholder "Program code", value model.program, onInput TextChange ] [] ],
    button [ onClick RunProgram ] [ text "Run Program" ]
    ]
