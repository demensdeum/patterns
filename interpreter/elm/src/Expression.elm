module Expression exposing (..)

type Expression =
  Print Expression -- Non terminal
  | Perform (List ExpressionLeaf) -- Non terminal
  | Constant String -- Terminal expressions
  | Set String String -- Terminal
  | Get String -- Terminal

type ExpressionLeaf =
  Node Expression