module Interpreter exposing (..)

import Context exposing (..)
import Expression exposing (..)
import Dict exposing (..)

type alias InterperterExpressionInput =
    {
      expression: Expression,
      context: Context
    }

type alias InterperterExpressionLeafInput =
    {
      expressionLeaf: ExpressionLeaf,
      context: Context
    }

type alias InterperterExpressionOutput =
  {
    output: String,
    context: Context
  }

run: InterperterExpressionInput -> InterperterExpressionOutput
run input = 
  case input.expression of
    Constant text ->
      { 
        output = text, 
        context = input.context 
      }
    Perform leafs ->
      let inputs = List.map (\leaf -> { expressionLeaf = leaf, context = input.context } ) leafs in
        let startLeaf = { expressionLeaf = (Node (Constant "")), context = { variables = Dict.empty } } in
          let outputExpressionInput = List.foldl mergeContextsAndRunLeafs startLeaf inputs in
            {
              output = (runExpressionLeaf outputExpressionInput).output,
              context = input.context
            }
    Print printExpression ->
      run 
      { 
        expression = printExpression, 
        context = input.context 
      }
    Set key value ->
      let variables = Dict.insert key value input.context.variables in
      {
        output = "OK",
        context = { variables = variables }
      }
    Get key ->
      {
        output = Maybe.withDefault ("No value for key: " ++ key) (Dict.get key input.context.variables),
        context = input.context
      }

mergeContextsAndRunLeafs: InterperterExpressionLeafInput -> InterperterExpressionLeafInput -> InterperterExpressionLeafInput
mergeContextsAndRunLeafs rhs lhs =
    let lhsOutput = runExpressionLeaf lhs in
      let rhsOutput = runExpressionLeaf { expressionLeaf = rhs.expressionLeaf, context = lhsOutput.context } in
        {
          expressionLeaf = Node (Constant (lhsOutput.output ++ "" ++ rhsOutput.output)),
          context = rhsOutput.context
        }

runExpressionLeaf: InterperterExpressionLeafInput -> InterperterExpressionOutput
runExpressionLeaf input =
  let expressionLeaf = input.expressionLeaf in
    case expressionLeaf of
      Node expression ->
        run {
          expression = expression,
          context = input.context
        }   