module Context exposing (Context)

import Dict exposing (..)

type alias Context = 
    {
        variables: Dict String String
    }