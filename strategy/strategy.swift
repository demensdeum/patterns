import Foundation

class MusicPlayer {
    var playerCodecAlgorithm: MusicPlayerCodecAlgorithm?
    
	func play(_ filePath: String) {
        playerCodecAlgorithm?.play(filePath)
	}
}

protocol MusicPlayerCodecAlgorithm {
    func play(_ filePath: String)
}

class MpegMusicPlayerCodecAlgorithm: MusicPlayerCodecAlgorithm {
	func play(_ filePath: String) {
		debugPrint("mpeg codec - play")
	}
}

class VorbisMusicPlayerCodecAlgorithm: MusicPlayerCodecAlgorithm {
	func play(_ filePath: String) {
		debugPrint("vorbis codec - play")	
	}
}

func play(fileAtPath path: String) {
		guard let url = URL(string: path) else { return }
		let fileExtension = url.pathExtension
		
		let musicPlayer = MusicPlayer()
		var playerCodecAlgorithm: MusicPlayerCodecAlgorithm? 
		
		if fileExtension == "mp3" {
            playerCodecAlgorithm = MpegMusicPlayerCodecAlgorithm()
		}
		else if fileExtension == "ogg" {
            playerCodecAlgorithm = VorbisMusicPlayerCodecAlgorithm()
		}
		
		musicPlayer.playerCodecAlgorithm = playerCodecAlgorithm
		musicPlayer.playerCodecAlgorithm?.play(path)
}

play(fileAtPath: "Djentuggah.mp3")
play(fileAtPath: "Procrastinallica.ogg")
