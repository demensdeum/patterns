import Foundation

protocol Originator {

    associatedtype T

    func memento() -> Memento<T>
    func apply(memento: Memento<T>)
}

class Memento<T> {
    public let date = Date()
    private let state: T
    
    init(state: T) {
        self.state = state
    }
    
    func requestState(sender: T) -> T {
        return state
    }
}
 
class MusicTrack: Originator {

    private var title: String

    init(title: String) {
        self.title = title
    }
    
    func memento() -> Memento<MusicTrack> {
        let state = MusicTrack(title: title)
        return Memento(state: state)
    }
    
    func apply(memento: Memento<MusicTrack>) {
        let state = memento.requestState(sender: self)
        title = state.title
    }
    
    func randomizeTitle() {
        title = "\(Int.random(in: 1...100))"
    }
    
    func printTitle() {
        print(title)
    }
}

let musicTrack = MusicTrack(title: "Procractinallica")
let memento = musicTrack.memento()
musicTrack.printTitle()
musicTrack.randomizeTitle()
musicTrack.printTitle()
musicTrack.apply(memento: memento)
musicTrack.printTitle()
