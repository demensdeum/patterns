function FSEFlowDelegate()
{
	this.didFinishEvent = function(sender) {}
}

function FSEMainController()
{
	this.didFinishEventSenderIdentfier = null;
	this.stateMachine = null;
	this.tapController = null;
	this.delegate = null;

	this.initialize = function(tapController)
	{
		this.tapController = tapController;

		this.stateMachine =  new FSEStateMachine()
		this.stateMachine.initialize();
	}

	this.register = function(iterable, state)
	{
		this.stateMachine.register(iterable, state);
	}

	this.start = function(state)
	{
		this.didFinishEventSenderIdentfier =  null;

		this.stateMachine.state = state;

		window.requestAnimationFrame(mainLoop);
	}

	this.step = function()
	{
		var iterable = this.stateMachine.iterable();

		if (iterable != null)
		{
			this.tapController.delegate = iterable;
			iterable.flowDelegate = this;

			iterable.step()

			if (this.didFinishEventSenderIdentfier == null)
			{
				window.requestAnimationFrame(mainLoop)
			}
			else
			{
				var didFinishEventSenderIdentfier = this.didFinishEventSenderIdentfier;
				this.delegate.mainControllerDidFinishEvent(this, this.didFinishEventSenderIdentfier);
			}
		}
		else
		{
			console.log("Bye-Bye!");
		}
	}

	this.didFinishEvent = function(sender)
	{
		this.didFinishEventSenderIdentfier =  sender.identifier;
	}

}

	this.mainLoop = function()
{

	globalMainController.step()

}

function FSEIdentifiable()
{
	this.identifier = null;
}

function FSEMainControllerDelegate()
{
	this.mainControllerDidFinishEvent = function(controller, senderIdentifier) {}
}

function FSEIterable()
{
	this.step = function() {}
}

function FSEStateMachine()
{

	this.state = null;
	this.stateToIterableMap = null;

	this.initialize = function()
	{
		this.stateToIterableMap =  {};
	}

	this.register = function(iterable, state)
	{
		this.stateToIterableMap[state] = iterable;
	}

	this.iterable = function()
	{
		return this.stateToIterableMap[this.state];
	}
}

function RDCreditsControllerScreen()
{
	this.framesCounter = null;
	this.render = null;
	this.flowDelegate = null;
	this.identifier = null;

	this.initialize = function(canvas, tapController)
	{
		this.identifier =  "Credits Controller Screen";

		this.tapController = tapController;

		this.render =  new CKRender()
		this.render.initialize(canvas)

		var demensdeumLogoImage = new Image();
		demensdeumLogoImage.src = "data/demensdeumLogo.png";

		var demensdeumLogo = new CKStaticImage();;
		demensdeumLogo.initialize(demensdeumLogoImage)

		this.render.add(demensdeumLogo);
	}

	this.step = function()
	{
		this.framesCounter += 1;

		if (this.framesCounter < 200)
		{
			this.render.render()
		}
		else
		{
			this.flowDelegate.didFinishEvent(this);
		}
	}

	this.tapControllerDidReceivedEvent = function(controller, tapEvent)
	{
		console.log("tapControllerDidReceivedEvent");

		this.flowDelegate.didFinishEvent(this);
	}

}

function IKTapController()
{

	this.delegate = null;

	this.initialize = function(canvas)
	{
		canvas.addEventListener("mouseup", globalTapController.touchUp, false);
		canvas.addEventListener("touchend", globalTapController.touchUp, false);
	}

	this.touchUp = function(event)
	{
		console.log("Touch up!");

		var tapEvent = new IKTapEvent();
		tapEvent.x = event.clientX;
		tapEvent.y = event.clientY;

		if (this.delegate != null) {
			this.delegate.tapControllerDidReceivedEvent(this, tapEvent);
		}
	}
}


function IKTapControllerDelegate()
{
	this.tapControllerDidReceivedEvent = function(tapEvent) {}
}

function IKTapEvent()
{
	this.x = null;
	this.y = null;
}

function CKRender()
{
	this.canvas = null;
	this.drawableObjects = null;

	this.initialize = function(canvas)
	{
		this.drawableObjects =  [];
		this.canvas = canvas;
	}

	this.render = function()
	{
		var strongThis = this;

		this.canvas.clear()

		this.drawableObjects.forEach(function (drawableObject, index) {

			strongThis.canvas.draw(drawableObject)

		});
	}

	this.add = function(drawableObject)
	{
		this.drawableObjects.push(drawableObject)
	}
}

function CKImage()
{
	this.fullImage = null;

	this.initialize = function(fullImage)
	{
		this.fullImage = fullImage;
	}

	this.width = function()
	{
		return this.fullImage.width;
	}

	this.height = function()
	{
		return this.fullImage.height;
	}

	this.image = function()
	{
		return this.fullImage;
	}
}

function CKStaticImage()
{

	this.drawableIdentifier = null;

	this.frame = null;
	this.image = null;

	this.alpha = null;

	this.initialize = function(image)
	{
		this.drawableIdentifier =  window.crypto.getRandomValues(new Uint32Array(4)).join('-');

		this.image =  new CKImage()
		this.image.initialize(image)

		this.frame =  new CKFrame(0, 0, this.image.width(), this.image.height())
		this.alpha =  1;
	}

}

function CKFrame()
{
	this.x = null;
	this.y = null;
	this.width = null;
	this.height = null;

	this.initialize = function(x, y, width, height)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

}

function CKVector()
{
	this.x = null;
	this.y = null;

	this.initialize = function(x, y)
	{
		this.x = x;
		this.y = y;
	}
}

function CKDrawable()
{
	this.drawableIdentifier = null;

	this.frame = null;
	this.image = null;

	this.alpha = null;
}

function CKCanvas()
{

	this.canvas = null;
	this.canvasContext = null;

	this.initialize = function(canvas)
	{
		this.canvas = canvas;
		this.canvasContext =  canvas.getContext('2d')
	}

	this.clear = function()
	{
		this.canvasContext.fillStyle = '#000000';
		this.canvasContext.fillRect(0, 0, canvas.width, canvas.height)
	}

	this.draw = function(drawable)
	{
		this.canvasContext.globalAlpha = drawable.alpha;
		this.canvasContext.drawImage(drawable.image.image(), drawable.frame.x, drawable.frame.y)
		this.canvasContext.globalAlpha = 1;
	}

}

function FWDemoControllerScreen()
{
	this.spriteModels = null;
	this.render = null;

	this.initialize = function(canvas, tapController)
	{

		this.spriteModels =  [];

		this.identifier = "Demo Controller Screen";

		this.tapController = tapController;

		this.render = new CKRender()
		this.render.initialize(canvas)

		var patternsLogoImage = new Image();
		patternsLogoImage.src = "data/patterns.png" ;

		var patternsLogoStaticImage = new CKStaticImage();
		patternsLogoStaticImage.initialize(patternsLogoImage)
		patternsLogoStaticImage.frame.y = 20;

		var catsCount = 40;

		var catImage = new Image();
		catImage.src = "data/cat.png";

		for (var i = 0; i < catsCount; i++) {;

			var catSprite = new CKStaticImage();
			catSprite.initialize(catImage)

			var xPosition = Math.random() * Math.floor(260);
			var yPosition = Math.random() * Math.floor(500);
			var spriteModel = new FWCat();
			var alpha = Math.random();
			spriteModel.initialize(catSprite, xPosition, yPosition, alpha);
			this.spriteModels.push(spriteModel)

			this.render.add(catSprite);
		}
		this.render.add(patternsLogoStaticImage)
	}

	this.step = function()
	{
		this.spriteModels.forEach(function (spriteModel, index) {
			spriteModel.step()
		});
		this.render.render()
	}

	this.tapControllerDidReceivedEvent = function(controller, tapEvent)
	{
		console.log("tapControllerDidReceivedEvent");
	}

}

function FWMainController()
{
	this.mainController = null;

	this.initialize = function(tapController)
	{
		var canvas = new CKCanvas();
		canvas.initialize(document.getElementById('canvas'))

		this.mainController =  new FSEMainController()
		this.mainController.initialize(tapController);
		this.mainController.delegate = this;

		var creditsControllerScreen = new RDCreditsControllerScreen();
		creditsControllerScreen.initialize(canvas);
		this.mainController.register(creditsControllerScreen, "Credits Screen");

		var demoControllerScreen = new FWDemoControllerScreen();
		demoControllerScreen.initialize(canvas);
		this.mainController.register(demoControllerScreen, "Demo Screen");
	}

	this.start = function()
	{
		this.mainController.start("Credits Screen");
	}

	this.step = function()
	{
		this.mainController.step()
	}

	this.mainControllerDidFinishEvent = function(controller, senderIdentifier)
	{
		console.log("mainControllerDidFinishEvent");

		if (senderIdentifier == "Credits Controller Screen")
		{
			this.mainController.start("Demo Screen");
		}
	}
}

function FWCat()
{

	this.staticImage = null;
	this.position = null;
	this.alpha = null;

	this.initialize = function(staticImage, x, y, alpha)
	{
		this.position =  new CKVector()
		this.position.initialize(x, y)
		this.alpha =  alpha;
		this.staticImage = staticImage;
	}

	this.step = function()
	{
		var alphaStep = 0.006;

		if (this.alpha > alphaStep) {
			this.alpha -= alphaStep;
		}
		else {
			this.position.x = Math.random() * Math.floor(260)
			this.position.y = Math.random() * Math.floor(500)
			this.alpha =  0.6 + Math.random()
			if (this.alpha > 1) {
				this.alpha =  1;
			}
		}
		this.staticImage.frame.x = this.position.x;
		this.staticImage.frame.y = this.position.y;
		this.staticImage.alpha = this.alpha;
	}
}

var main = function(arguments)
{
	globalTapController = new IKTapController()
	globalTapController.initialize(document.getElementById('canvas'))

	globalMainController = new FWMainController()
	globalMainController.initialize(globalTapController);
	globalMainController.start()
}


